/*
 * File: timetools.go
 * Project: Seavents
 * File Created: Saturday, 21st September 2019 11:51:51 pm
 * Author: Aurélien Castellarnau
 * Contact: (castellarnau.a@gmail.com)
 * -----
 * Last Modified: Sunday, 22nd September 2019 3:24:03 pm
 * Modified By: Aurélien Castellarnau
 * -----
 * Copyright © 2019 - 2019 Castellarnau Aurélien, castellarnau aurélien
 */

package timetools

import (
	"database/sql/driver"
	"time"
)

var (
	Layout        = "2006-01-02T15:04:05"
	NullValueTime = NullTime{Time: time.Time{}, Valid: false}
)

func GetParis() string {
	return "Europe/Paris"
}

/**
 * This pattern exist in goloang 1.13, package database/sql
 * gitlab ci sast security check support golang <= 1.12
 * to keep benefit of security check we keep this until gl-ci support 1.13
 * thanks to: https://github.com/lib/pq/blob/8c6ee72f3e6bcb1542298dd5f76cb74af9742cec/encode.go#L586
 */

// NullTime allow to securely manage a null value
// for a sql datetime
type NullTime struct {
	Time  time.Time
	Valid bool // Valid is true if Time is not NULL
}

// Scan implements the Scanner interface.
func (nt *NullTime) Scan(value interface{}) error {
	nt.Time, nt.Valid = value.(time.Time)
	return nil
}

// Value implements the driver Valuer interface.
func (nt NullTime) Value() (driver.Value, error) {
	if !nt.Valid {
		return nil, nil
	}
	return nt.Time, nil
}
