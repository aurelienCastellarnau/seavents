module seavents

go 1.13

require (
	github.com/go-sql-driver/mysql v1.4.1
	github.com/gorilla/mux v1.7.3
	github.com/stretchr/testify v1.4.0
)
