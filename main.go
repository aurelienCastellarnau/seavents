/*
 * File: main.go
 * Project: Seavents
 * File Created: Tuesday, 10th September 2019 5:24:42 pm
 * Author: Aurélien Castellarnau
 * Contact: (castellarnau.a@gmail.com)
 * -----
 * Last Modified: Tuesday, 10th September 2019 5:26:17 pm
 * Modified By: Aurélien Castellarnau
 * -----
 * Copyright © 2019 - 2019 Castellarnau Aurélien, castellarnau aurélien
 */

package main

import (
	"seavents/seavents"
)

func main() {
	seavents.Seavents()
}
