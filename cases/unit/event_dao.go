/*
 * File: event_dao_test.go
 * Project: Seavents
 * File Created: Tuesday, 17th September 2019 10:43:16 pm
 * Author: Aurélien Castellarnau
 * Contact: (castellarnau.a@gmail.com)
 * -----
 * Last Modified: Sunday, 22nd September 2019 5:35:56 pm
 * Modified By: Aurélien Castellarnau
 * -----
 * Copyright © 2019 - 2019 Castellarnau Aurélien, castellarnau aurélien
 */

package unit

import (
	"seavents/cases"
	"seavents/seavents/core/dao"
	"seavents/seavents/model"
	"seavents/timetools"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

// SelectEvents test data access to all events
func (u Tests) SelectEvents(t *testing.T) {
	events, err := cases.At.Database.EventsDAO().SelectEvents()
	assert.Nil(t, err, "selectEvents returned an error")
	mocked, err := cases.GetMock()
	assert.Nil(t, err, "GetMock returned an error")
	for key := range events {
		cases.AssertEvent(t, mocked[key], events[key], true)
	}
}

// READEventsDAOWithBadDatabase test data access to all events
// with a voluntary bad env making Database failed to connect
func (u Tests) READEventsDAOWithBadDatabase(t *testing.T) {
	// préparation du context d'erreur
	saveInstance := &dao.SQLInstance{
		Instance: cases.At.Database.Instance,
		DBErr:    cases.At.Database.DBErr,
	}
	cases.At.Database = GetBadDatabase()
	events, err := cases.At.Database.EventsDAO().SelectEvents()
	assert.NotNil(t, err, "selectEvents must return an error")
	assert.Contains(
		t,
		err.Error(),
		"dial tcp 123.4.5.6:789",
		"Error must concern the dial tcp attempt on the specified ip",
	)
	assert.Nil(t, events, "Events must be nil")
	// selectEventById
	event, err := cases.At.Database.EventsDAO().SelectEventByID(2)
	assert.NotNil(t, err, "selectEventByID must return an error")
	assert.Contains(
		t,
		err.Error(),
		"dial tcp 123.4.5.6:789",
		"Error must concern the dial tcp attempt on the specified ip",
	)
	assert.Equal(t, 0, event.ID, "Event ID must be equal to 0")
	cases.At.Database = saveInstance
}

func (u Tests) WRITEEventsDAOWithBadDatabase(t *testing.T) {
	// préparation du context d'erreur
	saveInstance := &dao.SQLInstance{
		Instance: cases.At.Database.Instance,
		DBErr:    cases.At.Database.DBErr,
	}
	cases.At.Database = GetBadDatabase()
	// insertEvent
	event, err := cases.At.Database.EventsDAO().InsertEvent(model.Events{Title: "fail"})
	assert.NotNil(t, err, "insertEvent must return an error")
	assert.Contains(
		t,
		err.Error(),
		"dial tcp 123.4.5.6:789",
		"Error must concern the dial tcp attempt on the specified ip",
	)
	assert.Equal(t, 0, event.ID, "Event ID must be equal to 0")
	// updateEvent
	event, err = cases.At.Database.EventsDAO().UpdateEvent(
		model.Events{ID: 2, Title: "fail"})
	assert.NotNil(t, err, "updateEvent must return an error")
	assert.Contains(
		t,
		err.Error(),
		"dial tcp 123.4.5.6:789",
		"Error must concern the dial tcp attempt on the specified ip",
	)
	assert.Equal(t, 0, event.ID, "Event ID must be equal to 0")
	// deleteEventByID
	err = cases.At.Database.EventsDAO().DeleteEventByID(2)
	assert.NotNil(t, err, "updateEvent must return an error")
	assert.Contains(
		t,
		err.Error(),
		"dial tcp 123.4.5.6:789",
		"Error must concern the dial tcp attempt on the specified ip",
	)
	cases.At.Database = saveInstance
}

// TestSelectEventByID test data access to event by id
func (u Tests) SelectEventByID(t *testing.T) {
	id := 2
	event, err := cases.At.Database.EventsDAO().SelectEventByID(id)
	assert.Nil(t, err, "selectEventById returned an error")
	// in a if clause to avoid panic on dereferencing
	assert.NotEqual(t, nil, event, "selected event can't be nil")
	mocked, err := cases.GetMock()
	assert.Nil(t, err, "GetMock returned an error")
	for key := range mocked {
		if mocked[key].ID == id {
			cases.AssertEvent(t, mocked[key], event, true)
		}
	}
}

// TestSelectEventByID test data access to event by id
func (u Tests) SelectEventByBadID(t *testing.T) {
	id := 0
	event, err := cases.At.Database.EventsDAO().SelectEventByID(id)
	assert.NotNil(t, err, "selectEventById returned an error")
	// in a if clause to avoid panic on dereferencing
	if assert.Equal(t, 0, event.ID, "selected event should be nil") &&
		assert.Equal(t, "", event.Title, "Data must be empty") {
		return
	}
}

// TestInsertEvent test data writing on event
func (u Tests) InsertEvent(t *testing.T) {
	nDate, err := time.Parse(timetools.Layout, "2008-08-08T08:08:08")
	assert.Nil(t, err, "instanciate new date")
	nEvent := model.Events{
		ID:     0,
		Title:  "Croisière Saint Johns' - Egypt",
		Date:   timetools.NullTime{Time: nDate, Valid: true},
		Start:  timetools.NullTime{Time: nDate, Valid: true},
		End:    timetools.NullTime{Time: nDate, Valid: true},
		AllDay: true,
	}
	check, err := cases.At.Database.EventsDAO().InsertEvent(nEvent)
	// in a if clause to avoid panic on dereferencing
	assert.Nil(t, err, "insertEvent returned an error")
	assert.NotNil(t, check.ID, "The new event object ID must be set after insert")
	cases.AssertEvent(t, nEvent, check, false)
}

// TestInsertEvent test data writing on event
func (u Tests) InsertBadEvent(t *testing.T) {
	nEvent := &model.Events{
		ID: 0,
		Title: `azertyuiopazertyuiopazertyuiopazertyuiopazertyuiopazertyuiop
		azertyuiopazertyuiopazertyuiopazertyuiopazertyuiopazertyuiopazertyuiop
		azertyuiopazertyuiopazertyuiopazertyuiopazertyuiopazertyuiopazertyuiop
		azertyuiopazertyuiopazertyuiopazertyuiopazertyuiopazertyuiopazertyuiop
		azertyuiopazertyuiopazertyuiopazertyuiopazertyuiopazertyuiopazertyuiop
		azertyuiopazertyuiopazertyuiopazertyuiopazertyuiopazertyuiopazertyuiop
		azertyuiopazertyuiopazertyuiopazertyuiopazertyuiopazertyuiopazertyuiop
		azertyuiopazertyuiopazertyuiopazertyuiopazertyuiopazertyuiopazertyuiop
		azertyuiopazertyuiopazertyuiopazertyuiopazertyuiopazertyuiopazertyuiop
		azertyuiopazertyuiopazertyuiopazertyuiopazertyuiopazertyuiopazertyuiop
		azertyuiopazertyuiopazertyuiopazertyuiopazertyuiopazertyuiopazertyuiop`,
	}
	check, err := cases.At.Database.EventsDAO().InsertEvent(*nEvent)
	// in a if clause to avoid panic on dereferencing
	if assert.NotNil(t, err, "insertEvent returned an error") &&
		assert.NotNil(t, err.Error(), "Data too long for column 'title' at row 1") &&
		assert.Equal(t, 0, check.ID, "selected event should be nil") &&
		assert.Equal(t, "", check.Title, "Data must be empty") {
		return
	}
}

// TestUpdateEvent test data writing on existing event
func (u Tests) UpdateEvent(t *testing.T) {
	mock, err := cases.GetMock()
	assert.Nil(t, err, "test update event returned an error")
	uEvent := mock[0]
	save := model.Events{
		ID:     uEvent.ID,
		Title:  uEvent.Title,
		Date:   uEvent.Date,
		Start:  uEvent.Start,
		End:    uEvent.End,
		AllDay: uEvent.AllDay,
	}
	uEvent.Title = "MST 2ème édition"
	nDate, err := time.Parse(time.RFC3339, "2008-08-08T08:08:08+02:00")
	assert.Nil(t, err, "instanciate new date")
	uEvent.Date = timetools.NullTime{Time: nDate, Valid: true}
	uEvent.Start = timetools.NullTime{Time: nDate, Valid: true}
	uEvent.End = timetools.NullTime{Time: nDate, Valid: true}
	uEvent.AllDay = !uEvent.AllDay
	check, err := cases.At.Database.EventsDAO().UpdateEvent(uEvent)
	// in a if clause to avoid panic on dereferencing check.ID if check is nil...
	assert.Nil(t, err, "updateEvent returned an error")
	assert.NotNil(t, check, "The edited event object can't be nil")
	assert.Equal(t, uEvent.ID, check.ID, "Returned edited object must have the samed ID")
	cases.AssertEvent(t, uEvent, check, true)
	cases.AssertDifferentEvent(t, save, check, false)
}

// TestInsertEvent test data writing on event
func (u Tests) UpdateBadEvent(t *testing.T) {
	tooLongNameEvent := &model.Events{
		ID: 1,
		Title: `azertyuiopazertyuiopazertyuiopazertyuiopazertyuiopazertyuiop
		azertyuiopazertyuiopazertyuiopazertyuiopazertyuiopazertyuiopazertyuiop
		azertyuiopazertyuiopazertyuiopazertyuiopazertyuiopazertyuiopazertyuiop
		azertyuiopazertyuiopazertyuiopazertyuiopazertyuiopazertyuiopazertyuiop
		azertyuiopazertyuiopazertyuiopazertyuiopazertyuiopazertyuiopazertyuiop
		azertyuiopazertyuiopazertyuiopazertyuiopazertyuiopazertyuiopazertyuiop
		azertyuiopazertyuiopazertyuiopazertyuiopazertyuiopazertyuiopazertyuiop
		azertyuiopazertyuiopazertyuiopazertyuiopazertyuiopazertyuiopazertyuiop
		azertyuiopazertyuiopazertyuiopazertyuiopazertyuiopazertyuiopazertyuiop
		azertyuiopazertyuiopazertyuiopazertyuiopazertyuiopazertyuiopazertyuiop
		azertyuiopazertyuiopazertyuiopazertyuiopazertyuiopazertyuiopazertyuiop`,
	}
	check, err := cases.At.Database.EventsDAO().UpdateEvent(*tooLongNameEvent)
	// in a if clause to avoid panic on dereferencing
	assert.NotNil(t, err, "updateEvent returned an error")
	assert.Equal(t, "Error 1406: Data too long for column 'title' at row 1", err.Error(), "Not the expected error message")
	assert.Equal(t, 0, check.ID, "updateEvent event should be nil")
	assert.Equal(t, "", check.Title, "Data must be empty")
	badIDEvent := &model.Events{
		ID:    0,
		Title: `azertyuiopazertyuiop`,
	}
	check, err = cases.At.Database.EventsDAO().UpdateEvent(*badIDEvent)
	// in a if clause to avoid panic on dereferencing
	assert.NotNil(t, err, "updateEvent returned an error")
	assert.Equal(t, "sql: no rows in result set", err.Error(), "Not the expected error message")
	assert.Equal(t, 0, check.ID, "updateEvent event should be nil")
	assert.Equal(t, "", check.Title, "Data must be empty")
}

// TestDeleteEventByID test data deletion on event by id
func (u Tests) DeleteEventByID(t *testing.T) {
	id := 2
	err := cases.At.Database.EventsDAO().DeleteEventByID(id)
	assert.Nil(t, err, "deleteEventByID returned an error")
}

// TestDeleteEventByID test data deletion on event by id
func (u Tests) DeleteEventByBadID(t *testing.T) {
	id := 0
	err := cases.At.Database.EventsDAO().DeleteEventByID(id)
	assert.NotNil(t, err, "deleteEventByID must return an error")
	assert.Equal(t, "sql: no rows in result set", err.Error(), "Not the expected error message")
}
