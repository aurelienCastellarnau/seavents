/* File: server.go
 * Project: Seavents
 * File Created: Tuesday, 10th September 2019 6:03:47 pm
 * Author: Aurélien Castellarnau
 * Contact: (castellarnau.a@gmail.com)
 * -----
 * Last Modified: Tuesday, 17th September 2019 12:21:04 pm
 * Last Modified: Tuesday, 17th September 2019 12:21:04 pm
 * Modified By: Aurélien Castellarnau
 * Copyright © 2019 - 2019 Castellarnau Aurélien, castellarnau aurélien
 */

package unit

import (
	"database/sql"
	"seavents/seavents/core/dao"

	//db driver library import
	_ "github.com/go-sql-driver/mysql"
)

// Tests regroup unitary testing
type Tests struct{}

// GetBadDatabase return a dao.Instance pointer with a failed connection
func GetBadDatabase() *dao.SQLInstance {
	Instance, sqlError := sql.Open("mysql", "bad:pwd@tcp(123.4.5.6:789)/bad_db?parseTime=true&timeout=5s")
	if sqlError == nil {
		sqlError = Instance.Ping()
	}
	return &dao.SQLInstance{
		Instance: Instance,
		DBErr:    sqlError,
	}
}
