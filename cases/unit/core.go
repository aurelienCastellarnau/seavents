/*
 * File: core_test.go
 * Project: Seavents
 * File Created: Tuesday, 17th September 2019 10:38:45 pm
 * Author: Aurélien Castellarnau
 * Contact: (castellarnau.a@gmail.com)
 * -----
 * Last Modified: Saturday, 21st September 2019 2:04:22 pm
 * Modified By: Aurélien Castellarnau
 * -----
 * Copyright © 2019 - 2019 Castellarnau Aurélien, castellarnau aurélien
 */

package unit

import (
	"seavents/seavents/core"
	"seavents/seavents/core/dao"
	"seavents/cases"
	"testing"
	"net/http"
	"fmt"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
)

// TestConfiguration test the content of core.Configuration after
func (u Tests) Configuration(t *testing.T) {
	assert.Equal(t, "4000", cases.At.Conf.Port, "Port in Configuration must be equal to")
	assert.Equal(t, "http://127.0.0.1:4000", cases.At.Conf.Addr, "Adress in Configuration must be equal to")
	if cases.At.Conf.Env == "gitlab" {
		assert.Equal(t, "mysql-test", cases.At.Conf.SQLHost, "In gitlab context SQLHost in Configuration must be equal to mysql-test")
	} else {
		assert.Equal(t, "127.0.0.1", cases.At.Conf.SQLHost, "In gitlab context SQLHost in Configuration must be equal to 127.0.0.1")
	}
	assert.Equal(t, "3306", cases.At.Conf.SQLPort, "SQLPort in Configuration must be equal to 3306")
	assert.Equal(t, "seavent_test_db", cases.At.Conf.DbName, "DbName in Configuration must be equal to seavent_test_db")
	assert.NotEmpty(t, cases.At.Conf.Env, "Env can't be empty")
	assert.Equal(t, "4000", cases.At.Conf.Port, "Port in Configuration must be equal to")
	assert.Equal(t, "http://127.0.0.1:4000", cases.At.Conf.Addr, "Adress in Configuration must be equal to")
}

// TestConfiguration test the content of core.Configuration after
func (u Tests) ConfigureOnAlreadyConfigured(t *testing.T) {
	duplicate := &core.App{}
	err := duplicate.Configure()
	assert.NotNil(t, err, "Trying to configure a second app must return an error")
	assert.Equal(t, "You can configure only one App", err.Error(), "Error message must clearly indicate that you are instanciating a second app")
}

// TestReconfiguration test the content of core.Configuration after an override
func (u Tests) Reconfiguration(t *testing.T) {
	actualConfiguration := cases.At.Conf
	confSnapshot := &core.Configuration{
		Env:     actualConfiguration.Env,
		Addr:    actualConfiguration.Addr,
		Port:    actualConfiguration.Port,
		SQLHost: actualConfiguration.SQLHost,
		SQLPort: actualConfiguration.SQLPort,
		DbName:  actualConfiguration.DbName,
		SQLUser: actualConfiguration.SQLUser,
		SQLPwd:  actualConfiguration.SQLPwd,
	}
	old, err := cases.At.Reconfigure(core.Configuration{
		Env:     "dev",
		Addr:    "unknow",
		Port:    "unknow",
		SQLHost: "unknow",
		SQLPort: "unknow",
		DbName:  "unknow",
		SQLUser: "unknow",
		SQLPwd:  "unknow",
	})
	assert.Nil(t, err, "reconfiguration must not return an error")
	assert.Equal(t, confSnapshot.Env, old.Env, "Old env must be unchanged")
	assert.Equal(t, confSnapshot.Port, old.Port, "Old Port in Configuration must be unchanged")
	assert.Equal(t, confSnapshot.Addr, old.Addr, "Old Adress in Configuration must be unchanged")
	assert.Equal(t, confSnapshot.SQLHost, old.SQLHost, "Old SQLHost must be unchanged")
	assert.Equal(t, confSnapshot.SQLPort, old.SQLPort, "Old SQLPort in Configuration must be unchanged")
	assert.Equal(t, confSnapshot.DbName, old.DbName, "Old DbName in Configuration must be unchanged")
	assert.Equal(t, confSnapshot.SQLUser, old.SQLUser, "Old SQLUser in Configuration must be unchanged")
	assert.Equal(t, confSnapshot.SQLPwd, old.SQLPwd, "Old SQLPwd in Configuration must be unchanged")
	assert.NotNil(t, actualConfiguration, "New Configuration can't be empty")
	assert.Equal(t, "dev", actualConfiguration.Env, "actualConfiguration Env must be equal to unknow")
	assert.NotEqual(t, "4000", actualConfiguration.Port, "Reconfigured Port must be different than the old one")
	assert.NotEqual(t, "http://127.0.0.1:4000", actualConfiguration.Addr, "Reconfigured Addr must be different than the old one")
	assert.NotEqual(t, old.SQLHost, actualConfiguration.SQLHost, "Reconfigured SQLHost must be different than the old one")
	assert.NotEqual(t, old.SQLPort, actualConfiguration.SQLPort, "Reconfigured SQLPort must be different than the old one")
	assert.NotEqual(t, old.DbName, actualConfiguration.DbName, "Reconfigured DbName must be different than the old one")
	assert.NotEqual(t, old.SQLUser, actualConfiguration.SQLUser, "Reconfigured SQLUser must be different than the old one")
	assert.NotEqual(t, old.SQLPwd, actualConfiguration.SQLPwd, "Reconfigured SQLPwd must be different than the old one")
	_, err = cases.At.Reconfigure(*old)
	assert.Nil(t, err, "reconfiguration must not return an error")
}

// TestBadReconfiguration test the content of Configuration after an override
func (u Tests) BadReconfiguration(t *testing.T) {
	Conf := &core.Configuration{
		Env:     cases.At.Conf.Env,
		Addr:    cases.At.Conf.Addr,
		Port:    cases.At.Conf.Port,
		SQLHost: cases.At.Conf.SQLHost,
		SQLPort: cases.At.Conf.SQLPort,
		DbName:  cases.At.Conf.DbName,
		SQLUser: cases.At.Conf.SQLUser,
		SQLPwd:  cases.At.Conf.SQLPwd,
	}
	old, err := cases.At.Reconfigure(core.Configuration{
		Env:     "unknow",
		Addr:    "unknow",
		Port:    "unknow",
		SQLHost: "unknow",
		SQLPort: "unknow",
		DbName:  "unknow",
		SQLUser: "unknow",
		SQLPwd:  "unknow",
	})
	assert.NotNil(t, err, "bad reconfiguration must return an error")
	assert.Equal(t, err.Error(), "Env must be equal to prod | dev | test | gitlab")
	assert.Equal(t, Conf.Env, old.Env, "Old env must be unchanged")
	assert.Equal(t, Conf.Port, old.Port, "Old Port in core.Configuration must be unchanged")
	assert.Equal(t, Conf.Addr, old.Addr, "Adress in Configuration must be equal to old one")
	assert.Equal(t, Conf.SQLHost, old.SQLHost, "Old SQLHost must be unchanged")
	assert.Equal(t, Conf.SQLPort, old.SQLPort, "Old SQLPort in Configuration must be unchanged")
	assert.Equal(t, Conf.DbName, old.DbName, "Old DbName in Configuration must be unchanged")
	assert.Equal(t, Conf.SQLUser, old.SQLUser, "Old SQLUser in Configuration must be unchanged")
	assert.Equal(t, Conf.SQLPwd, old.SQLPwd, "Old SQLPwd in Configuration must be unchanged")
	_, err = cases.At.Reconfigure(*old)
	assert.Nil(t, err, "reconfiguration shouldnt return an error")
}

// TestRouter provide a basic router testing
func (u Tests) Router(t *testing.T) {
	customHandler := func(a *core.App, w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "%s", "test road")
	}
	r := cases.At.CreateRouting(map[string][]core.Road{
		"/testing": []core.Road{
			{Title: "test1", Method: "GET", Pattern: "/one", Handler: customHandler},
		},
		"/testing2": []core.Road{
			{Title: "test2", Method: "GET", Pattern: "/two", Handler: customHandler},
		},
		"/testing3": []core.Road{
			{Title: "test3", Method: "GET", Pattern: "/three", Handler: customHandler},
		},
	})
	err := r.Walk(func(route *mux.Route, router *mux.Router, ancestors []*mux.Route) error {
		_, err := route.GetPathTemplate()
		assert.Nil(t, err, "We should retrieve the router path template without error")
		return nil
	})
	assert.Nil(t, err, "We should traverse the router without error")
}

// TestDatabase check the connection to Database
func (u Tests) Database(t *testing.T) {
	db := &dao.SQLInstance{}
	db.Instance, db.DBErr = dao.Connect(cases.At.BuildDSN())
	assert.Nil(t, db.DBErr, "Error must be nil")
	assert.NotEqual(t, nil, db.Instance, "Must be different than")
	err := db.Close()
	assert.Nil(t, err, "Error must be nil")
}

// TestBadDatabaseEnv check the connection to Database with bad env
func (u Tests) BadDatabaseConnection(t *testing.T) {
	old, err := cases.At.Reconfigure(core.Configuration{SQLHost: "unknow"})
	assert.Nil(t, err, "Reconfigure shouldnt return an error")
	db, err := dao.Connect(cases.At.BuildDSN())
	assert.NotNil(t, err, "Trying to connect to a Database with bad host must return an error")
	assert.Nil(t, nil, db, "The Database Instance must be nil")
	_, err = cases.At.Reconfigure(*old)
	assert.Nil(t, err, "Reconfigure shouldnt return an error")
}
