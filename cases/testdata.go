/*
 * File: RebootData.go
 * Project: Seavents
 * File Created: Tuesday, 17th September 2019 8:23:59 pm
 * Author: Aurélien Castellarnau
 * Contact: (castellarnau.a@gmail.com)
 * -----
 * Last Modified: Sunday, 22nd September 2019 6:55:41 am
 * Modified By: Aurélien Castellarnau
 * -----
 * Copyright © 2019 - 2019 Castellarnau Aurélien, castellarnau aurélien
 */

package cases

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"seavents/seavents/core"
	"seavents/seavents/core/dao"
	"seavents/seavents/model"
	"seavents/timetools"
	"strings"
	"testing"
	"time"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
)

var (
	databaseName = "seavent_test_db"
	// At Application tester
	At *core.App
	// Rt Router tester
	Rt *mux.Router
)

func AssertDifferentEvent(t *testing.T, mock, response model.Events, withID bool) {
	if withID {
		assert.NotEqual(t, mock.ID, response.ID, "Response.ID and mock.ID must be different")
	}
	assert.NotEqual(t, mock.Title, response.Title, "Response.Title and mock.Title must be different")
	assert.NotEqual(t, mock.Date.Time.Format(timetools.Layout), response.Date.Time.Format(timetools.Layout), "Response.Date and mock.Date must be different")
	assert.NotEqual(t, mock.Start.Time.Format(timetools.Layout), response.Start.Time.Format(timetools.Layout), "Response.Start and mock.Start must be different")
	assert.NotEqual(t, mock.End.Time.Format(timetools.Layout), response.End.Time.Format(timetools.Layout), "Response.End and mock.End must be different")
	assert.NotEqual(t, mock.AllDay, response.AllDay, "Response.AllDay and mock.AllDay must be different")
}

func AssertEvent(t *testing.T, mock, response model.Events, withID bool) {
	if withID {
		assert.Equal(t, mock.ID, response.ID, "Response.ID and mock.ID must be equal")
	}
	assert.Equal(t, mock.Title, response.Title, "Response.Title and mock.Title must be equal")
	assert.Equal(t, mock.Date.Time.Format(timetools.Layout), response.Date.Time.Format(timetools.Layout), "Response.Date and mock.Date must be equal")
	assert.Equal(t, mock.Start.Time.Format(timetools.Layout), response.Start.Time.Format(timetools.Layout), "Response.Start and mock.Start must be equal")
	assert.Equal(t, mock.End.Time.Format(timetools.Layout), response.End.Time.Format(timetools.Layout), "Response.End and mock.End must be equal")
	assert.Equal(t, mock.AllDay, response.AllDay, "Response.AllDay and mock.AllDay must be equal")
}

// RebootData use the actual Database connection to reset the content of 'databaseName'
// a file named 'databaseNamed'.sql is loaded in the process
func RebootData(a *core.App) error {
	// Locally, nothing guaranty the existence of the test Database. Here, a call on getSQLConnection is unsafe
	if a.Conf.Env == "test" {
		old, err := a.Reconfigure(core.Configuration{SQLUser: "root", SQLPwd: "root", SQLHost: "127.0.0.1", DbName: "anon"})
		if err != nil {
			return err
		}
		defer func() {
			_, err = a.Reconfigure(*old)
			if err != nil {
				fmt.Print(err.Error())
			}
		}()
		a.Database.Instance, a.Database.DBErr = dao.Connect(a.BuildDSN())
	}
	if a.Database.DBErr != nil {
		return a.Database.DBErr
	}
	wd, err := os.Getwd()
	if err != nil {
		return err
	}
	// preparing data context
	_, err = a.Database.Instance.Exec("DROP DATABASE IF EXISTS " + databaseName)
	if err != nil {
		return err
	}
	_, err = a.Database.Instance.Exec("CREATE DATABASE " + databaseName)
	if err != nil {
		return err
	}
	_, err = a.Database.Instance.Exec("USE " + databaseName)
	if err != nil {
		return err
	}
	path := filepath.Join(wd, fmt.Sprintf("sql/%s.sql", databaseName))
	// clean: https://github.com/cloudfoundry/bosh-softlayer-cpi-release/issues/304
	c, ioErr := ioutil.ReadFile(filepath.Clean(path))
	if ioErr != nil {
		return ioErr
	}
	// Splitting on ';' we execute each query one by one
	queries := strings.SplitAfter(string(c), ";")
	for _, query := range queries {
		// Sometimes, the last item is an empty string
		if strings.Trim(query, " ") != "" {
			_, err = a.Database.Instance.Exec(query)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

// GetMock give a struct representation of the testing context
func GetMock() ([]model.Events, error) {
	date, err := time.Parse(timetools.Layout, "2007-07-07T07:07:07")
	if err != nil {
		fmt.Printf("\nError: %s", err.Error())
		return nil, err
	}
	ntDate := timetools.NullTime{Time: date, Valid: true}
	return []model.Events{
		{
			ID:     1,
			Title:  "Marseille Shark Tour",
			Date:   ntDate,
			Start:  ntDate,
			End:    ntDate,
			AllDay: false,
		},
		{
			ID:     2,
			Title:  "BDE - Egypt",
			Date:   ntDate,
			Start:  ntDate,
			End:    ntDate,
			AllDay: false,
		},
		{
			ID:     3,
			Title:  "White Shark South Africa",
			Date:   ntDate,
			Start:  ntDate,
			End:    ntDate,
			AllDay: false,
		},
	}, err
}
