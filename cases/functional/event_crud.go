/*
 * File: event_crud.go
 * Project: Seavents
 * File Created: Wednesday, 18th September 2019 3:39:41 pm
 * Author: Aurélien Castellarnau
 * Contact: (castellarnau.a@gmail.com)
 * -----
 * Last Modified: Tuesday, 24th September 2019 4:26:48 pm
 * Modified By: Aurélien Castellarnau
 * -----
 * Copyright © 2019 - 2019 Castellarnau Aurélien, castellarnau aurélien
 */

package functional

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"path/filepath"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	//db driver library import
	"seavents/cases"
	"seavents/seavents/core"
	"seavents/seavents/core/dao"
	"seavents/seavents/model"
	"seavents/timetools"

	_ "github.com/go-sql-driver/mysql"
)

/**
 * In all tests against our server, we create a context good or propice to error,
 * we create a request object, a recorder wich record the server response and we check the recorder content.
 */

var url = "http://localhost:4000"

type EventCrud struct{}
type testRoad struct {
	method  string
	pattern string
	body    io.Reader
}

// serve user httptest content to simulate a call to our server
// return byte[] format of body and recorder
func (ec EventCrud) serve(t *testing.T, method, url string, body io.Reader) ([]byte, *httptest.ResponseRecorder) {
	req, err := http.NewRequest(method, url, body)
	assert.Nil(t, err, "")
	assert.NotEqual(t, req, nil, "Body of request can't be nil")
	recorder := httptest.NewRecorder()
	// We serve the request with our rt router
	cases.Rt.ServeHTTP(recorder, req)
	bodyContent, err := ioutil.ReadAll(recorder.Body)
	assert.Nil(t, err, "")
	return bodyContent, recorder
}

func (ec EventCrud) assertSeaventError(t *testing.T, code int, message string, body []byte) {
	seaventError := &core.SeaventsError{}
	err := json.Unmarshal(body, seaventError)
	assert.Nil(t, err, "")
	assert.Equal(t, code, seaventError.Code, "seavents error code must be %d", code)
	assert.Equal(t, message, seaventError.Message, "Wrong message")
}

func (ec EventCrud) assertSeaventErrorContain(t *testing.T, code int, contain string, body []byte) {
	seaventError := &core.SeaventsError{}
	err := json.Unmarshal(body, seaventError)
	assert.Nil(t, err, "")
	assert.Equal(t, code, seaventError.Code, "seavents error code must be %d", code)
	assert.Contains(t, seaventError.Message, contain, "message must contain")
}

func (ec EventCrud) TestGETEvents(t *testing.T) {
	defer cases.RebootData(cases.At)
	// Request/Recorder creation
	body, recorder := ec.serve(t, "GET", url+"/events", nil)
	assert.Equal(t, 200, recorder.Code, "Http code must be 200")
	assert.NotEqual(t, recorder.Body, nil, "Body of response can't be nil")

	mock, err := cases.GetMock()
	assert.Nil(t, err, "GetMock returned error")
	var response []model.Events

	err = json.Unmarshal(body, &response)
	assert.Nil(t, err, "Must be nil")
	assert.Equal(t, 3, len(response), "There is 3 events in db")

	for i := 0; i < len(response); i++ {
		cases.AssertEvent(t, mock[i], response[i], true)
	}
}

func (ec EventCrud) TestEventsCRUDWithBadDatabase(t *testing.T) {
	// Bad context creation
	old, err := cases.At.Reconfigure(core.Configuration{SQLUser: "bad", SQLPwd: "user", SQLHost: "unknow", DbName: "anon"})
	defer func() {
		_, err = cases.At.Reconfigure(*old)
		if err != nil {
			log.Fatal(err.Error())
		}
		cases.At.Database.Instance, cases.At.Database.DBErr = dao.Connect(cases.At.BuildDSN())
		err = cases.RebootData(cases.At)
		if err != nil {
			log.Fatal(err.Error())
		}
	}()
	cases.At.Database.Instance, cases.At.Database.DBErr = dao.Connect(cases.At.BuildDSN())

	date, err := time.Parse(timetools.Layout, "2009-09-09T09:09:09")
	assert.Nil(t, err, "")
	// body preparation
	pEvent := model.Events{
		Title:  "Functionnal Creation",
		Date:   timetools.NullTime{Time: date, Valid: true},
		Start:  timetools.NullTime{Time: date, Valid: true},
		End:    timetools.NullTime{Time: date, Valid: true},
		AllDay: false,
	}
	uEvent := model.Events{
		ID:     1,
		Title:  "Functionnal Update",
		Date:   timetools.NullTime{Time: date, Valid: true},
		Start:  timetools.NullTime{Time: date, Valid: true},
		End:    timetools.NullTime{Time: date, Valid: true},
		AllDay: false,
	}
	postBody, err := json.Marshal(pEvent)
	assert.Equal(t, err, nil, "Must be equal to")
	putBody, err := json.Marshal(uEvent)
	assert.Equal(t, err, nil, "Must be equal to")
	// We test that all roads react the same with a bad database
	for _, r := range []testRoad{
		{method: "GET", pattern: url + "/events", body: nil},
		{method: "GET", pattern: url + "/events/2", body: nil},
		{method: "POST", pattern: url + "/events", body: bytes.NewReader(postBody)},
		{method: "PUT", pattern: url + "/events", body: bytes.NewReader(putBody)},
		{method: "DELETE", pattern: url + "/events/2", body: nil},
	} {
		// Request/Recorder creation + serve
		body, recorder := ec.serve(t, r.method, r.pattern, r.body)
		assert.Equal(t, 500, recorder.Code, "Http error code must be 500")
		// Error unmarsharlling
		ec.assertSeaventErrorContain(t, 500, "dial", body)
	}
}

func (ec EventCrud) TestGETEvent(t *testing.T) {
	defer cases.RebootData(cases.At)
	// mock
	mock, err := cases.GetMock()
	assert.Nil(t, err, "GetMock returned an error")
	// Request/Recorder creation + serve
	body, recorder := ec.serve(t, "GET", url+"/events/1", nil)

	// check code and body not empty
	assert.Equal(t, recorder.Code, 200, "Must be equal to")
	assert.NotEqual(t, recorder.Body, nil, "Must be different of")

	var response model.Events

	// Body part of the recorder
	err = json.Unmarshal(body, &response)
	assert.Nil(t, err, "Unmarshal response must happen nicely")
	assert.NotEqual(t, response, nil, "The response of GET /event/1 shouldnt be nil")
	cases.AssertEvent(t, mock[0], response, true)
}

func (ec EventCrud) TestGETEventWithBadID(t *testing.T) {
	defer cases.RebootData(cases.At)
	// Request/Recorder creation + serve
	body, _ := ec.serve(t, "GET", url+"/events/a", nil)
	// error expectation
	ec.assertSeaventError(t, 400, "strconv.Atoi: parsing \"a\": invalid syntax", body)
}

func (ec EventCrud) TestPOSTEvents(t *testing.T) {
	defer cases.RebootData(cases.At)
	date, err := time.Parse(timetools.Layout, "2009-09-09T09:09:09")
	assert.Nil(t, err, "")
	event := model.Events{
		Title:  "Functionnal Creation",
		Date:   timetools.NullTime{Time: date, Valid: true},
		Start:  timetools.NullTime{Time: date, Valid: true},
		End:    timetools.NullTime{Time: date, Valid: true},
		AllDay: true,
	}
	// We serve the request with our testApp router
	body, err := json.Marshal(event)
	assert.Equal(t, err, nil, "Must be equal to")

	// Request/Recorder creation + serve
	body, recorder := ec.serve(t, "POST", url+"/events", bytes.NewReader(body))
	assert.Equal(t, 201, recorder.Code, "Http code must be 201")
	assert.NotNil(t, recorder.Body, "body shouldnt be empty")

	var response model.Events

	err = json.Unmarshal(body, &response)
	assert.Nil(t, err, "Unmarshal response must happen nicely")
	assert.NotNil(t, response, "response shouldnt be nil")
	assert.NotEqual(t, response.ID, event.ID, "response.ID shouldnt be equal to input event.ID")
	cases.AssertEvent(t, event, response, false)
}

func (ec EventCrud) TestPOSTBadEvents(t *testing.T) {
	defer cases.RebootData(cases.At)
	date, err := time.Parse(timetools.Layout, "2009-09-09T09:09:09")
	assert.Nil(t, err, "")
	// event with too long title for database
	event := model.Events{
		Title: `IamTooLongIamTooLongIamTooLongIamTooLongIamTooLongIamTooLong
		 IamTooLongIamTooLongIamTooLongIamTooLongIamTooLongIamTooLong
		 IamTooLongIamTooLongIamTooLongIamTooLongIamTooLongIamTooLong
		 IamTooLongIamTooLongIamTooLongIamTooLongIamTooLongIamTooLong
		 IamTooLongIamTooLongIamTooLongIamTooLongIamTooLongIamTooLong
		 IamTooLongIamTooLongIamTooLongIamTooLongIamTooLongIamTooLong
		 IamTooLongIamTooLongIamTooLongIamTooLongIamTooLongIamTooLong
		 IamTooLongIamTooLongIamTooLongIamTooLongIamTooLongIamTooLong
		 IamTooLongIamTooLongIamTooLongIamTooLongIamTooLongIamTooLong
		 IamTooLongIamTooLongIamTooLongIamTooLongIamTooLongIamTooLong
		 IamTooLongIamTooLongIamTooLongIamTooLongIamTooLongIamTooLong`,
		Date:   timetools.NullTime{Time: date, Valid: true},
		Start:  timetools.NullTime{Time: date, Valid: true},
		End:    timetools.NullTime{Time: date, Valid: true},
		AllDay: true,
	}
	// We serve the request with our testApp router
	reqBody, err := json.Marshal(event)
	assert.Nil(t, err, "Json Marshal must happen nicely")
	body, _ := ec.serve(t, "POST", url+"/events", bytes.NewReader(reqBody))
	// Error unmarsharlling
	ec.assertSeaventError(t, 500, "Error 1406: Data too long for column 'title' at row 1", body)
}

func (ec EventCrud) TestPOSTEmptyEvents(t *testing.T) {
	defer cases.RebootData(cases.At)
	// we serve and get a recorder
	body, _ := ec.serve(t, "POST", url+"/events", nil)
	// Error unmarsharlling
	ec.assertSeaventError(t, 500, "missing form body", body)
}

func (ec EventCrud) TestPOSTEventsBadFormat(t *testing.T) {
	defer cases.RebootData(cases.At)
	// we serve and get a body
	body, _ := ec.serve(t, "POST", url+"/events", bytes.NewReader([]byte("{bad: 'objet'}")))
	// Error unmarsharlling
	ec.assertSeaventError(t, 500, "invalid character 'b' looking for beginning of object key string", body)
}

func (ec EventCrud) TestPOSTEventsTooBigBody(t *testing.T) {
	defer cases.RebootData(cases.At)
	path := ""
	if cases.At.Conf.Env == "gitlab" {
		wd, err := os.Getwd()
		assert.Nil(t, err, "")
		path = filepath.Join(filepath.Dir(wd) + "/cases/135ko.txt")
	} else {
		path = filepath.Join(os.Getenv("GOPATH"), "/src/seavents/cases/135ko.txt")
	}
	veryBigName, err := ioutil.ReadFile(path)
	assert.Nil(t, err, "")

	event := model.Events{
		Title: string(veryBigName),
	}
	// We serve the request with our testApp router
	reqBody, err := json.Marshal(event)
	assert.Nil(t, err, "")
	// We serve the request with our testApp router
	body, _ := ec.serve(t, "POST", url+"/events", bytes.NewReader(reqBody))
	ec.assertSeaventError(t, 400, "Maximum body size of  128 kb reached", body)
}

func (ec EventCrud) TestPUTEvents(t *testing.T) {
	defer cases.RebootData(cases.At)
	nDate, err := time.Parse(timetools.Layout, "2008-08-08T08:08:08")
	assert.Nil(t, err, "Parsing date must happen nicely")
	event := model.Events{
		ID:     1,
		Title:  "Functionnal Update",
		Date:   timetools.NullTime{Time: nDate, Valid: true},
		Start:  timetools.NullTime{Time: nDate, Valid: true},
		End:    timetools.NullTime{Time: nDate, Valid: true},
		AllDay: true,
	}
	// We serve the request with our testApp router
	reqBody, err := json.Marshal(event)
	assert.Equal(t, err, nil, "Marshal event into body shouldnt return an error")

	body, recorder := ec.serve(t, "PUT", url+"/events", bytes.NewReader(reqBody))
	assert.Equal(t, 201, recorder.Code, "http code must be equal to 201")
	assert.NotNil(t, recorder.Body, "body shouldnt be nil")

	var response model.Events
	// We transfert the recorder Body into our response of type facilitys
	err = json.Unmarshal(body, &response)
	assert.Nil(t, err, "Unmarshal body into Events shouldnt return error")
	assert.NotNil(t, response, "Response shouldnt be nil")
	cases.AssertEvent(t, event, response, true)
}

func (ec EventCrud) TestPUTEmptyEvents(t *testing.T) {
	defer cases.RebootData(cases.At)
	body, recorder := ec.serve(t, "PUT", url+"/events", nil)
	assert.Equal(t, 500, recorder.Code, "Http code must be 500")
	assert.NotNil(t, recorder.Body, "body shouldnt be empty")
	ec.assertSeaventError(t, 500, "missing form body", body)
}

func (ec EventCrud) TestPUTBadEvents(t *testing.T) {
	defer cases.RebootData(cases.At)
	nDate, err := time.Parse(timetools.Layout, "2008-08-08T08:08:08")
	event := model.Events{
		ID: 1,
		Title: `IamTooLongIamTooLongIamTooLongIamTooLongIamTooLongIamTooLong
		 IamTooLongIamTooLongIamTooLongIamTooLongIamTooLongIamTooLong
		 IamTooLongIamTooLongIamTooLongIamTooLongIamTooLongIamTooLong
		 IamTooLongIamTooLongIamTooLongIamTooLongIamTooLongIamTooLong
		 IamTooLongIamTooLongIamTooLongIamTooLongIamTooLongIamTooLong
		 IamTooLongIamTooLongIamTooLongIamTooLongIamTooLongIamTooLong
		 IamTooLongIamTooLongIamTooLongIamTooLongIamTooLongIamTooLong
		 IamTooLongIamTooLongIamTooLongIamTooLongIamTooLongIamTooLong
		 IamTooLongIamTooLongIamTooLongIamTooLongIamTooLongIamTooLong
		 IamTooLongIamTooLongIamTooLongIamTooLongIamTooLongIamTooLong
		 IamTooLongIamTooLongIamTooLongIamTooLongIamTooLongIamTooLong`,
		Date:   timetools.NullTime{Time: nDate, Valid: true},
		Start:  timetools.NullTime{Time: nDate, Valid: true},
		End:    timetools.NullTime{Time: nDate, Valid: true},
		AllDay: true,
	}
	// We serve the request with our testApp router
	reqBody, err := event.MarshalJSON()
	assert.Equal(t, err, nil, "Must be equal to")

	body, recorder := ec.serve(t, "PUT", url+"/events", bytes.NewReader(reqBody))
	assert.Equal(t, 500, recorder.Code, "Http code must be 500")
	assert.NotNil(t, recorder.Body, "body shouldnt be empty")
	ec.assertSeaventError(t, 500, "Error 1406: Data too long for column 'title' at row 1", body)
}

func (ec EventCrud) TestPUTEventsTooBigBody(t *testing.T) {
	defer cases.RebootData(cases.At)
	path := ""
	wd, err := os.Getwd()
	assert.Nil(t, err, "")
	if cases.At.Conf.Env == "gitlab" {
		path = filepath.Join(filepath.Dir(wd) + "/cases/135ko.txt")
	} else {
		path = filepath.Join(os.Getenv("GOPATH"), "/src/seavents/cases/135ko.txt")
	}
	veryBigName, err := ioutil.ReadFile(path)
	assert.Nil(t, err, "")

	event := model.Events{
		Title: string(veryBigName),
	}
	// We serve the request with our testApp router
	reqBody, err := json.Marshal(event)
	assert.Nil(t, err, "")

	// serve
	body, recorder := ec.serve(t, "PUT", url+"/events", bytes.NewReader(reqBody))
	assert.Equal(t, 400, recorder.Code, "Http code must be 400")
	assert.NotEqual(t, nil, recorder.Body, "body shouldnt be empty")
	ec.assertSeaventError(t, 400, "Maximum body size of  128 kb reached", body)
}

func (ec EventCrud) TestDELETEEvent(t *testing.T) {
	defer cases.RebootData(cases.At)
	// serve
	body, recorder := ec.serve(t, "DELETE", url+"/events/1", nil)
	assert.Equal(t, 200, recorder.Code, "http code must be equal to 200")
	assert.NotNil(t, recorder.Body, "recorder Body cant be nil")
	successResponse := &core.SeaventsSuccess{}
	err := json.Unmarshal(body, successResponse)
	assert.Nil(t, err, "Unmarshal successReponse must happen nicely")
	assert.Equal(t, "successfully deleted", successResponse.Message, "A successfull deletion must return a success message")
}

func (ec EventCrud) TestDELETEEventWitBadID(t *testing.T) {
	defer cases.RebootData(cases.At)
	// serve
	body, recorder := ec.serve(t, "DELETE", url+"/events/a", nil)
	assert.Equal(t, 400, recorder.Code, "http code must be equal to 200")
	assert.NotNil(t, recorder.Body, "recorder Body cant be nil")
	ec.assertSeaventError(t, 400, "strconv.Atoi: parsing \"a\": invalid syntax", body)
}
