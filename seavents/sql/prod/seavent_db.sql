CREATE DATABASE IF NOT EXISTS seavent_db;
USE seavent_db;
DROP TABLE IF EXISTS events;
CREATE TABLE events (
    id smallint unsigned not null auto_increment,
    title varchar(250) NOT NULL,
    date Datetime NOT NULL,
    start Datetime DEFAULT NULL,
    end Datetime DEFAULT NULL,
    allday boolean DEFAULT FALSE,
    constraint pk_events primary key (id)
);
INSERT INTO events ( id, title, date, start, end, allday ) VALUES
( null, 'Marseille Shark Tour', '2019-07-01T09:00:00', '2019-07-01T09:00:00', '2019-09-01T18:00:00', false),
( null, 'Marseille Shark Tour 2', '2019-07-08T09:00:00', '2019-07-08T09:00:00', '2019-09-08T18:00:00', false),
( null, 'BDE - Egypt','2019-10-05T07:00:00', '2019-10-05T07:00:00', '2019-10-12T17:00:00', false),
( null, 'White Shark South Africa', '2019-10-14T07:00:00', '2019-10-14T07:00:00', '2019-10-21T17:00:00', false);