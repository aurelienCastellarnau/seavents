CREATE DATABASE IF NOT EXISTS seavent_test_db;
USE seavent_test_db;
DROP TABLE IF EXISTS events;
CREATE TABLE events (
    id smallint unsigned not null auto_increment,
    title varchar(250) NOT NULL,
    date Datetime NOT NULL,
    start Datetime DEFAULT NULL,
    end Datetime DEFAULT NULL,
    allday boolean DEFAULT FALSE,
    constraint pk_events primary key (id)
);
INSERT INTO events ( id, title, date, start, end, allday ) VALUES
( null, 'Marseille Shark Tour', '2007-07-07T07:07:07', '2007-07-07T07:07:07', '2007-07-07T07:07:07', false),
( null, 'BDE - Egypt','2007-07-07T07:07:07', '2007-07-07T07:07:07', '2007-07-07T07:07:07', false),
( null, 'White Shark South Africa', '2007-07-07T07:07:07', '2007-07-07T07:07:07', '2007-07-07T07:07:07', false);