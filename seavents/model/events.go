/*
 * File: event_model.go
 * Project: Seavents
 * File Created: Tuesday, 10th September 2019 1:08:10 am
 * Author: Aurélien Castellarnau
 * Contact: (castellarnau.a@gmail.com)
 * -----
 * Last Modified: Tuesday, 24th September 2019 4:26:07 pm
 * Modified By: Aurélien Castellarnau
 * -----
 * Copyright © 2019 - 2019 Castellarnau Aurélien, castellarnau aurélien
 */

package model

import (
	"encoding/json"
	"regexp"
	"seavents/timetools"
	"strings"
	"time"

	//db driver library import
	_ "github.com/go-sql-driver/mysql"
)

// Events define a seavent event shape
type Events struct {
	ID     int                `json:"id"`
	Title  string             `json:"title"`
	Date   timetools.NullTime `json:"date"`
	Start  timetools.NullTime `json:"start,omitempty"`
	End    timetools.NullTime `json:"end,omitempty"`
	AllDay bool               `json:"allDay"`
}

// MarshalJSON interface. will be call to encode Events
func (e Events) MarshalJSON() ([]byte, error) {
	type Alias Events
	return json.Marshal(&struct {
		Date   string `json:"date"`
		Start  string `json:"start,omitempty"`
		End    string `json:"end,omitempty"`
		ID     int    `json:"id"`
		Title  string `json:"title"`
		AllDay bool   `json:"allDay"`
	}{
		ID:     e.ID,
		Title:  e.Title,
		AllDay: e.AllDay,
		Date:   e.Date.Time.Format(timetools.Layout),
		Start:  e.Start.Time.Format(timetools.Layout),
		End:    e.End.Time.Format(timetools.Layout),
	})
}

// UnmarshalJSON interface. will be call to decode Events
func (e *Events) UnmarshalJSON(data []byte) error {
	type Alias Events
	aux := &struct {
		Date   string `json:"date"`
		Start  string `json:"start,omitempty"`
		End    string `json:"end,omitempty"`
		ID     int    `json:"id"`
		Title  string `json:"title"`
		AllDay bool   `json:"allDay"`
	}{}
	if err := json.Unmarshal(data, &aux); err != nil {
		return err
	}

	// asitis fields
	e.ID = aux.ID
	e.Title = aux.Title
	e.AllDay = aux.AllDay

	// Specific fields

	// date
	date, err := time.Parse(timetools.Layout, parseDate(aux.Date))
	if err != nil {
		return err
	}
	e.Date = timetools.NullTime{Time: date, Valid: true}

	// start
	start, err := time.Parse(timetools.Layout, parseDate(aux.Start))
	if err != nil {
		e.Start = timetools.NullValueTime
	} else {
		e.Start = timetools.NullTime{Time: start, Valid: true}
	}

	// end
	end, err := time.Parse(timetools.Layout, parseDate(aux.End))
	if err != nil {
		e.End = timetools.NullValueTime
	} else {
		e.End = timetools.NullTime{Time: end, Valid: true}
	}
	return nil
}

func parseDate(date string) string {
	d := strings.Split(date, "Z")[0]
	// d = strings.Join(strings.Split(d, "T"), " ")
	d = strings.Split(d, ".")[0]
	matched, err := regexp.Match(`^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}$`, []byte(d))
	if err != nil || !matched {
		return ""
	}
	return d
}
