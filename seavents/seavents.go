/*
 * File: seavents.go
 * Project: Seavents
 * File Created: Monday, 9th September 2019 6:43:17 pm
 * Author: Aurélien Castellarnau
 * Contact: (castellarnau.a@gmail.com)
 * -----
 * Last Modified: Sunday, 22nd September 2019 6:35:50 pm
 * Modified By: Aurélien Castellarnau
 * -----
 * Copyright © 2019 - 2019 Castellarnau Aurélien, castellarnau aurélien
 */

package seavents

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"seavents/seavents/core"
)

// PROJECTNAME is a constant defining the name of the project
var PROJECTNAME = "seavents"

// Seavents init the serveur for Seavent App
func Seavents() {
	a := &core.App{}
	// configure the App using flags
	err := a.Configure()
	if nil != err {
		log.Fatalf("Error during Seavents Configuration: %s", err.Error())
	}
	defer func() {
		err = a.Database.Close()
		if nil != err {
			fmt.Printf("Error during sql connection closing: %s", err.Error())
			os.Exit(1)
		}
	}()
	// print essential of Configuration
	fmt.Printf(`
			Server configuration:

		address: %s
		env: %s
		sql host: %s
		sql port: %s
		sql user: %s
		sql pwd: %s
		sql database: %s
		sql timeout: %s

	`, a.Conf.Addr, a.Conf.Env, a.Conf.SQLHost, a.Conf.SQLPort, a.Conf.SQLUser, a.Conf.SQLPwd, a.Conf.DbName, a.Conf.SQLTimeout)
	// use of log.Fatal to print and close clean after the execution of ListenAndServe
	// with our port and our router, App.CreateRouting receive a map of []Road indexed by a path
	// it allow to separate, configuration/Database and router/handlers parts
	// we could accept another section with a completely different data access
	log.Fatal(http.ListenAndServe(":"+a.Conf.Port, a.CreateRouting(map[string][]core.Road{
		"/events": []core.Road{
			{Title: "fetch all events", Pattern: "", Method: "GET", Handler: core.GetEvents},
			{Title: "fetch one event by id", Pattern: "/{id}", Method: "GET", Handler: core.GetEventByID},
			{Title: "insert one event", Pattern: "", Method: "POST", Handler: core.PostEvent},
			{Title: "update one event", Pattern: "", Method: "PUT", Handler: core.PutEvent},
			{Title: "delete one event by id", Pattern: "/{id}", Method: "DELETE", Handler: core.DelEventByID},
		},
	},
	)))
}
