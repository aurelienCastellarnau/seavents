/*
 * File: seavents_test.go
 * Project: Seavents
 * File Created: Monday, 9th September 2019 8:39:07 pm
 * Author: Aurélien Castellarnau
 * Contact: (castellarnau.a@gmail.com)
 * -----
 * Last Modified: Sunday, 22nd September 2019 6:35:32 pm
 * Modified By: Aurélien Castellarnau
 * -----
 * Copyright © 2019 - 2019 Castellarnau Aurélien, castellarnau aurélien
 */

package seavents

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"seavents/cases"
	"seavents/cases/functional"
	"seavents/cases/unit"
	"seavents/seavents/core"
	"strconv"
	"testing"

	//db driver library import
	_ "github.com/go-sql-driver/mysql"
)

var (
	databaseName = "seavent_test_db"
)

// TestBatch is a shortcut for the string/func tests container
type TestBatch map[string]func(*testing.T)

func executeBatch(t *testing.T, batch TestBatch) {
	fmt.Print("\nTests launch:\n")
	for testCase, test := range batch {
		cases.RebootData(cases.At)
		fmt.Printf("\n%s\n", testCase)
		test(t)
	}
}

// listenTryAndRetry will launch tests on defined port
// if it failed, 5 attemtps on the 5 next ports will be trigger
func listenTryAndRetry() {
	var err error
	var count int
	intPort, err := strconv.Atoi(cases.At.Conf.Port)
	count = intPort
	if nil != err {
		return
	}
	for {
		count++
		err = http.ListenAndServe(":"+strconv.Itoa(count), cases.At.CreateRouting(map[string][]core.Road{
			"events": []core.Road{
				{Title: "fetch all events", Pattern: "/", Method: "GET", Handler: core.GetEvents},
				{Title: "fetch one event by id", Pattern: "/{id}", Method: "GET", Handler: core.GetEventByID},
				{Title: "insert one event", Pattern: "/", Method: "POST", Handler: core.PostEvent},
				{Title: "update one event", Pattern: "/", Method: "PUT", Handler: core.PutEvent},
				{Title: "delete one event by id", Pattern: "/{id}", Method: "DELETE", Handler: core.DelEventByID},
			},
		},
		))
		if count-intPort >= 5 && err != nil {
			break
		}
	}
	log.Fatal(err)
}

func TestMain(m *testing.M) {
	cases.At = &core.App{}
	err := cases.At.Configure()
	if nil != err {
		fmt.Printf("Error during test Configuration: %s", err.Error())
		os.Exit(1)
	}
	defer func() {
		err = cases.At.Database.Close()
		if nil != err {
			fmt.Printf("Error during sql connection closing: %s", err.Error())
			os.Exit(1)
		}
	}()
	cases.RebootData(cases.At)
	cases.Rt = cases.At.CreateRouting(map[string][]core.Road{
		"/events": []core.Road{
			{Title: "fetch all events", Pattern: "", Method: "GET", Handler: core.GetEvents},
			{Title: "fetch one event by id", Pattern: "/{id}", Method: "GET", Handler: core.GetEventByID},
			{Title: "insert one event", Pattern: "", Method: "POST", Handler: core.PostEvent},
			{Title: "update one event", Pattern: "", Method: "PUT", Handler: core.PutEvent},
			{Title: "delete one event by id", Pattern: "/{id}", Method: "DELETE", Handler: core.DelEventByID},
		},
	},
	)

	/*
		go listenTryAndRetry()
		time.Sleep(2 * time.Second)
	*/
	os.Exit(m.Run())
}

func TestCore(t *testing.T) {
	ut := unit.Tests{}
	executeBatch(t, TestBatch{
		"TestConfiguration":                ut.Configuration,
		"TestConfigureOnAlreadyConfigured": ut.ConfigureOnAlreadyConfigured,
		"TestReconfiguration":              ut.Reconfiguration,
		"TestBadReconfiguration":           ut.BadReconfiguration,
		"TestRouter":                       ut.Router,
		"TestDatabase":                     ut.Database,
		"TestBadDatabaseConnection":        ut.BadDatabaseConnection,
	})
}

func TestEventDao(t *testing.T) {
	ut := unit.Tests{}
	executeBatch(t, TestBatch{
		"SelectEvents":                  ut.SelectEvents,
		"READEventsDAOWithBadDatabase":  ut.READEventsDAOWithBadDatabase,
		"WRITEEventsDAOWithBadDatabase": ut.WRITEEventsDAOWithBadDatabase,
		"SelectEventByID":               ut.SelectEventByID,
		"SelectEventByBadID":            ut.SelectEventByBadID,
		"InsertEvent":                   ut.InsertEvent,
		"InsertBadEvent":                ut.InsertBadEvent,
		"UpdateEvent":                   ut.UpdateEvent,
		"UpdateBadEvent":                ut.UpdateBadEvent,
		"DeleteEventByID":               ut.DeleteEventByID,
		"DeleteEventByBadID":            ut.DeleteEventByBadID,
	})
}

func TestEventCrud(t *testing.T) {
	ft := functional.EventCrud{}
	executeBatch(t, TestBatch{
		"TestGETEvents":                 ft.TestGETEvents,
		"TestGETEvent":                  ft.TestGETEvent,
		"TestGETEventWithBadID":         ft.TestGETEventWithBadID,
		"TestEventsCRUDWithBadDatabase": ft.TestEventsCRUDWithBadDatabase,
		"TestPOSTEvents":                ft.TestPOSTEvents,
		"TestPOSTBadEvents":             ft.TestPOSTBadEvents,
		"TestPOSTEmptyEvents":           ft.TestPOSTEmptyEvents,
		"TestPOSTEventsBadFormat":       ft.TestPOSTEventsBadFormat,
		"TestPOSTEventsTooBigBody":      ft.TestPOSTEventsTooBigBody,
		"TestPUTEvents":                 ft.TestPUTEvents,
		"TestPUTBadEvents":              ft.TestPUTBadEvents,
		"TestPUTEmptyEvents":            ft.TestPUTEmptyEvents,
		"TestPUTEventsTooBigBody":       ft.TestPUTEventsTooBigBody,
		"TestDELETEEvent":               ft.TestDELETEEvent,
		"TestDELETEEventWitBadID":       ft.TestDELETEEventWitBadID,
	})
}
