/*
 * File: benchmark.
 * Project: Seavents
 * File Created: Friday, 13th September 2019 9:37:42 pm
 * Author: Aurélien Castellarnau
 * Contact: (castellarnau.a@gmail.com)
 * -----
 * Last Modified: Sunday, 22nd September 2019 6:36:26 pm
 * Modified By: Aurélien Castellarnau
 * -----
 * Copyright © 2019 - 2019 Castellarnau Aurélien, castellarnau aurélien
 */

package seavents

import (
	"fmt"
	"net/http"
	"seavents/cases"
	"seavents/seavents/core"
	"seavents/seavents/core/dao"
	"seavents/seavents/model"
	"testing"
)

// BenchmarkConfigure benchmark configure method
func BenchmarkConfigure(b *testing.B) {
	for n := 0; n < b.N; n++ {
		err := cases.At.Configure()
		if nil != err {
			return
		}
	}
}

/*
func BenchmarkCheckAndSetEnv(b *testing.B) {
	for n := 0; n < b.N; n++ {
		err := checkEnv("test", &Configuration{})
		if nil != err {
			return
		}
	}
}
*/
func BenchmarkReconfigure(b *testing.B) {
	for n := 0; n < b.N; n++ {
		_, err := cases.At.Reconfigure(core.Configuration{Env: "test"})
		if nil != err {
			return
		}
	}
}
func BenchmarkCreateRouting(b *testing.B) {
	customHandler := func(a *core.App, w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "%s", "test road")
	}
	for n := 0; n < b.N; n++ {
		_ = cases.At.CreateRouting(map[string][]core.Road{
			"testing": []core.Road{
				{Title: "test1", Method: "GET", Pattern: "/one", Handler: customHandler},
			},
			"testing2": []core.Road{
				{Title: "test2", Method: "GET", Pattern: "/two", Handler: customHandler},
			},
			"testing3": []core.Road{
				{Title: "test3", Method: "GET", Pattern: "/three", Handler: customHandler},
			},
		})
	}
}
func BenchmarkConnect(b *testing.B) {
	for n := 0; n < b.N; n++ {
		_, err := dao.Connect(cases.At.BuildDSN())
		if nil != err {
			return
		}
	}
}
func BenchmarkGetEventsDao(b *testing.B) {
	db := &dao.SQLInstance{}
	db.Instance, db.DBErr = dao.Connect(cases.At.BuildDSN())
	for n := 0; n < b.N; n++ {
		_ = db.EventsDAO()
	}
}
func BenchmarkSelectEvents(b *testing.B) {
	for n := 0; n < b.N; n++ {
		_, err := cases.At.Database.EventsDAO().SelectEvents()
		if nil != err {
			return
		}
	}
}
func BenchmarkSelectEventByID(b *testing.B) {
	for n := 0; n < b.N; n++ {
		_, err := cases.At.Database.EventsDAO().SelectEventByID(2)
		if nil != err {
			return
		}
	}
}
func BenchmarkInsertEvent(b *testing.B) {
	for n := 0; n < b.N; n++ {
		_, err := cases.At.Database.EventsDAO().InsertEvent(model.Events{})
		if nil != err {
			return
		}
	}
}
func BenchmarkUpdateEvent(b *testing.B) {
	for n := 0; n < b.N; n++ {
		_, err := cases.At.Database.EventsDAO().UpdateEvent(model.Events{})
		if nil != err {
			return
		}
	}
}
func BenchmarkDeleteEventByID(b *testing.B) {
	for n := 0; n < b.N; n++ {
		err := cases.At.Database.EventsDAO().DeleteEventByID(2)
		if nil != err {
			return
		}
	}
}
