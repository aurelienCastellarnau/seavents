/*
 * File: configure.go
 * Project: agopora
 * File Created: Sunday, 14th April 2019 4:13:35 pm
 * Author: Aurélien Castellarnau
 * Contact: (castellarnau.a@gmail.com)
 * -----
 * Last Modified: Tuesday, 24th September 2019 6:19:12 pm
 * Modified By: Aurélien Castellarnau
 * -----
 * Copyright © 2018 - 2019 Castellarnau Aurélien, castellarnau aurélien
 */

package core

import (
	"flag"
	"fmt"
	"seavents/seavents/core/dao"
	"sync"

	"github.com/gorilla/mux"
)

var (
	localhost = "127.0.0.1"
	apiPort   = "4000"
	flagOnce  sync.Once
	// At is a global empty instance of App for tests
	At *App
	// Rt is a global empty instance of *mux.Router for tests
	Rt *mux.Router
)

// App define the majors parts of a web application
type App struct {
	Conf     *Configuration
	Database *dao.SQLInstance
}

// Configuration define app's necessaries properties
type Configuration struct {
	Env         string
	Addr        string
	Port        string
	SQLHost     string
	SQLPort     string
	DbName      string
	SQLUser     string
	SQLPwd      string
	SQLTimeout  string
	MaxBodySize int64
}

func (a *App) checkEnv() error {
	if a.Conf.Env != "prod" && a.Conf.Env != "dev" && a.Conf.Env != "test" && a.Conf.Env != "gitlab" {
		return fmt.Errorf("Env must be equal to prod | dev | test | gitlab")
	}
	return nil
}

// Configure manage flag calls and return the Configuration with default/user values
func (a *App) Configure() error {
	flagOnce.Do(func() {
		var ip = flag.String("ip", localhost, "define ip address for API")
		var port = flag.String("p", apiPort, "define port for API")
		var env = flag.String("env", "dev", "Environnement, default to dev, can be gitlab, test (localhost test) or prod (localhost)")
		var sqlHost = flag.String("sqlh", localhost, "define the host for sql database connection")
		var sqlPort = flag.String("sqlp", "3306", "define the port for sql database connection")
		var sqlUser = flag.String("sqlu", "root", "define the user for sql database connection")
		var sqlPassword = flag.String("sqlpwd", "root", "define the password for sql database connection")
		var dbName = flag.String("db", "seavent_db", "define the host for sql database connection")
		var sqlTimeout = flag.String("sqlto", "5", "define the timeout in sec for sql database connection")
		var maxBodySize = flag.Int64("bodysize", 128, "define the maxmimum accepted body size in kb")
		flag.Parse()
		a.Conf = &Configuration{
			Env:         *env,
			Addr:        "http://" + *ip + ":" + *port,
			Port:        *port,
			SQLHost:     *sqlHost,
			SQLPort:     *sqlPort,
			DbName:      *dbName,
			SQLUser:     *sqlUser,
			SQLPwd:      *sqlPassword,
			SQLTimeout:  *sqlTimeout,
			MaxBodySize: *maxBodySize,
		}
	})
	if a.Conf == nil {
		return fmt.Errorf("You can configure only one App")
	}
	err := a.checkEnv()
	if nil != err {
		return err
	}
	db, err := dao.Connect(a.BuildDSN())
	a.Database = &dao.SQLInstance{
		Instance: db,
		DBErr:    err,
	}
	return nil
}

// BuildDSN provide the data source name corresponding to the current App.Configuration state
func (a *App) BuildDSN() string {
	return fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?parseTime=true&timeout=%ss",
		a.Conf.SQLUser, a.Conf.SQLPwd, a.Conf.SQLHost, a.Conf.SQLPort, a.Conf.DbName, a.Conf.SQLTimeout)
}

// Reconfigure allow to override the actual Configuration
// incomplete Configuration struct accepted
func (a *App) Reconfigure(input Configuration) (old *Configuration, err error) {
	save := &Configuration{
		Env:         a.Conf.Env,
		Addr:        a.Conf.Addr,
		Port:        a.Conf.Port,
		SQLHost:     a.Conf.SQLHost,
		SQLPort:     a.Conf.SQLPort,
		DbName:      a.Conf.DbName,
		SQLUser:     a.Conf.SQLUser,
		SQLPwd:      a.Conf.SQLPwd,
		SQLTimeout:  a.Conf.SQLTimeout,
		MaxBodySize: a.Conf.MaxBodySize,
	}
	if input.Env != "" {
		// error declaration is shadowed by return declaration
		a.Conf.Env = input.Env
		err = a.checkEnv()
		if err != nil {
			return save, err
		}
	}
	if input.Addr != "" {
		a.Conf.Addr = input.Addr
	}
	if input.Port != "" {
		a.Conf.Port = input.Port
	}
	if input.SQLHost != "" {
		a.Conf.SQLHost = input.SQLHost
	}
	if input.SQLPort != "" {
		a.Conf.SQLPort = input.SQLPort
	}
	// we want database name can be null
	if input.DbName != "" {
		if input.DbName == "anon" {
			a.Conf.DbName = ""
		} else {
			a.Conf.DbName = input.DbName
		}
	}
	if input.SQLUser != "" {
		a.Conf.SQLUser = input.SQLUser
	}
	if input.SQLPwd != "" {
		a.Conf.SQLPwd = input.SQLPwd
	}
	if input.SQLTimeout != "" {
		a.Conf.SQLTimeout = input.SQLTimeout
	}
	if input.MaxBodySize != 0 {
		a.Conf.MaxBodySize = input.MaxBodySize
	}
	return save, nil
}
