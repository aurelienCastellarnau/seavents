/*
 * File: event_handler.go
 * Project: Seavents
 * File Created: Tuesday, 10th September 2019 5:41:25 pm
 * Author: Aurélien Castellarnau
 * Contact: (castellarnau.a@gmail.com)
 * -----
 * Last Modified: Saturday, 21st September 2019 6:34:12 pm
 * Modified By: Aurélien Castellarnau
 * -----
 * Copyright © 2019 - 2019 Castellarnau Aurélien, castellarnau aurélien
 */

package core

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"seavents/seavents/model"
	"strconv"

	"github.com/gorilla/mux"
)

// GetEvents is a CustomHandler asking a select of all Events
func GetEvents(a *App, w http.ResponseWriter, req *http.Request) {
	// Access Data
	events, err := a.Database.EventsDAO().SelectEvents()
	if nil != err {
		handleError(500, err, w)
		return
	}
	// Format Data
	response, err := json.Marshal(events)
	if nil != err {
		handleError(500, err, w)
		return
	}
	w.WriteHeader(200)
	// Write Data
	fmt.Fprintf(w, "%s", response)
}

// GetEventByID is a CustomHandler asking a select of an identified Events
func GetEventByID(a *App, w http.ResponseWriter, req *http.Request) {
	// how to get url parameter with mux
	vars := mux.Vars(req)
	// how to convert string parameter to int
	id, err := strconv.Atoi(vars["id"])
	if nil != err {
		handleError(400, err, w)
		return
	}
	event, err := a.Database.EventsDAO().SelectEventByID(id)
	if nil != err {
		handleError(500, err, w)
		return
	}
	// Format Data
	response, err := json.Marshal(event)
	if nil != err {
		handleError(500, err, w)
		return
	}
	w.WriteHeader(200)
	// Write Data
	fmt.Fprintf(w, "%s", response)
}

// DelEventByID is a CustomHandler asking a delete of an identified Events
func DelEventByID(a *App, w http.ResponseWriter, req *http.Request) {
	// how to get url parameter with mux
	vars := mux.Vars(req)
	// how to convert string parameter to int
	id, err := strconv.Atoi(vars["id"])
	if nil != err {
		handleError(400, err, w)
		return
	}
	err = a.Database.EventsDAO().DeleteEventByID(id)
	if nil != err {
		handleError(500, err, w)
		return
	}
	// Format Data
	response, err := json.Marshal(SeaventsSuccess{Message: "successfully deleted"})
	if nil != err {
		handleError(500, err, w)
		return
	}
	w.WriteHeader(200)
	// Write Data
	fmt.Fprintf(w, "%s", response)
}

// PostEvent is a CustomHandler asking an insert of a provided Events
func PostEvent(a *App, w http.ResponseWriter, r *http.Request) {
	var input model.Events

	// Body part of the request
	err := r.ParseForm()
	if err != nil {
		handleError(500, err, w)
		return
	}
	// using copy allow to protect direct memory used instead of ioutil.ReadAll
	// io.Copy use 32kb buffer to transfert data, at the end we reject body bigger
	// than Configuration.MaxBodySize
	body := &bytes.Buffer{}
	written, err := io.Copy(body, r.Body)
	if err != nil {
		handleError(500, err, w)
		return
	}
	if written > a.Conf.MaxBodySize*1000 {
		handleError(400, fmt.Errorf("%s %d %s", "Maximum body size of ", a.Conf.MaxBodySize, "kb reached"), w)
		return
	}
	defer r.Body.Close()
	err = json.Unmarshal(body.Bytes(), &input)
	if err != nil {
		handleError(500, err, w)
		return
	}
	nEvent, err := a.Database.EventsDAO().InsertEvent(input)
	if err != nil {
		handleError(500, err, w)
		return
	}
	response, err := json.Marshal(nEvent)
	if err != nil {
		handleError(500, err, w)
		return
	}
	// we send back response
	w.WriteHeader(201)
	fmt.Fprintf(w, "%s", response)
}

// PutEvent is a CustomHandler asking an update of a provided Events
func PutEvent(a *App, w http.ResponseWriter, r *http.Request) {
	var input model.Events

	// Body part of the request
	err := r.ParseForm()
	if err != nil {
		handleError(500, err, w)
		return
	}
	body := &bytes.Buffer{}
	written, err := io.Copy(body, r.Body)
	if err != nil {
		handleError(500, err, w)
		return
	}
	if written > a.Conf.MaxBodySize*1000 {
		handleError(400, fmt.Errorf("%s %d %s", "Maximum body size of ", a.Conf.MaxBodySize, "kb reached"), w)
		return
	}
	// we defer the call to close
	defer r.Body.Close()
	err = json.Unmarshal(body.Bytes(), &input)
	if err != nil {
		handleError(500, err, w)
		return
	}
	nEvent, err := a.Database.EventsDAO().UpdateEvent(input)
	if err != nil {
		handleError(500, err, w)
		return
	}
	response, err := json.Marshal(nEvent)
	if err != nil {
		handleError(500, err, w)
		return
	}
	// we send back response
	w.WriteHeader(201)
	fmt.Fprintf(w, "%s", response)
}
