/*
 * File: Database.go
 * Project: Seavents
 * File Created: Tuesday, 10th September 2019 1:00:15 am
 * Author: Aurélien Castellarnau
 * Contact: (castellarnau.a@gmail.com)
 * -----
 * Last Modified: Sunday, 22nd September 2019 4:41:32 pm
 * Modified By: Aurélien Castellarnau
 * -----
 * Copyright © 2019 - 2019 Castellarnau Aurélien, castellarnau aurélien
 */

package dao

import (
	"database/sql"

	//db driver library import
	_ "github.com/go-sql-driver/mysql"
)

const dsn = "root:root@tcp(any-but-not-localhost:3306)/seavent_db?parseTime=true&timeout=5s"

//SQLInstance is an object to get db informations
type SQLInstance struct {
	Instance *sql.DB
	DBErr    error
}

// Connect use the provided dsn to establish connection
// and ping the Database to check it
func Connect(dsn string) (*sql.DB, error) {
	var db *sql.DB
	var err error

	db, err = sql.Open("mysql", dsn)
	if err == nil && db != nil {
		err = db.Ping()
		if err != nil {
			return nil, err
		}
	}
	return db, err
}

// Close is a shortcut to *sql.DB.Close
func (db *SQLInstance) Close() error {
	if db.Instance != nil {
		err := db.Instance.Ping()
		if nil != err {
			return err
		}
		return db.Instance.Close()
	}
	return nil
}

// EventsDAO return a 'Database provided' EventsDao
func (db *SQLInstance) EventsDAO() *EventsDao {
	return &EventsDao{
		conn: db,
	}
}
