/*
 * File: event_dao.go
 * Project: Seavents
 * File Created: Tuesday, 10th September 2019 5:45:23 pm
 * Author: Aurélien Castellarnau
 * Contact: (castellarnau.a@gmail.com)
 * -----
 * Last Modified: Sunday, 22nd September 2019 4:41:38 pm
 * Modified By: Aurélien Castellarnau
 * -----
 * Copyright © 2019 - 2019 Castellarnau Aurélien, castellarnau aurélien
 */

package dao

import (
	"fmt"
	"seavents/seavents/model"
	"seavents/timetools"
)

// EventsDao hold the connection Instance and provide data accessors
type EventsDao struct {
	conn *SQLInstance
}

// SelectEvents proceed a select all against the Database
func (dao *EventsDao) SelectEvents() ([]model.Events, error) {
	var events []model.Events

	if nil != dao.conn.DBErr {
		return nil, dao.conn.DBErr
	}
	// fullfill a rows struct with query result
	rows, err := dao.conn.Instance.Query("SELECT * FROM events")
	if nil != err {
		return nil, err
	}
	for rows.Next() {
		var event model.Events
		// fill an model.Events item with data
		err = rows.Scan(
			&event.ID,
			&event.Title,
			&event.Date,
			&event.Start,
			&event.End,
			&event.AllDay)
		if err != nil {
			// untestable error...
			return nil, err
		}
		// compound []model.Events
		events = append(events, event)
	}
	return events, nil
}

// SelectEventByID perform a prepared SELECT statement against SQL db
func (dao *EventsDao) SelectEventByID(id int) (model.Events, error) {
	var event model.Events

	err := dao.conn.DBErr
	if nil != err {
		return model.Events{}, err
	}
	// fullfill a rows struct with query result
	err = dao.conn.Instance.QueryRow("SELECT * FROM events WHERE id=?", id).Scan(
		&event.ID,
		&event.Title,
		&event.Date,
		&event.Start,
		&event.End,
		&event.AllDay,
	)
	if nil != err {
		return model.Events{}, err
	}
	return event, nil
}

// InsertEvent perform a prepared INSERT statement against SQL db
func (dao *EventsDao) InsertEvent(input model.Events) (model.Events, error) {
	err := dao.conn.DBErr
	if nil != err {
		return model.Events{}, err
	}
	add, err := dao.conn.Instance.Prepare("INSERT INTO events(title, date, start, end, allday) VALUES(?, ?, ?, ?, ?)")
	if err != nil {
		return model.Events{}, err
	}
	res, err := add.Exec(
		input.Title,
		input.Date.Time.Format(timetools.Layout),
		input.Start.Time.Format(timetools.Layout),
		input.End.Time.Format(timetools.Layout),
		input.AllDay,
	)
	if err != nil {
		return model.Events{}, err
	}
	lastID, err := res.LastInsertId()
	if err != nil {
		return model.Events{}, err
	}
	return dao.SelectEventByID(int(lastID))
}

// UpdateEvent perform a prepared UPDATE statement against SQL db
func (dao *EventsDao) UpdateEvent(input model.Events) (model.Events, error) {
	err := dao.conn.DBErr
	if nil != err {
		return model.Events{}, err
	}
	_, err = dao.SelectEventByID(input.ID)
	if nil != err {
		return model.Events{}, err
	}
	edit, err := dao.conn.Instance.Prepare("UPDATE events SET title=?, date=?, start=?, end=?, allday=? WHERE id=?")
	if err != nil {
		return model.Events{}, err
	}
	_, err = edit.Exec(
		input.Title,
		input.Date.Time.Format(timetools.Layout),
		input.Start.Time.Format(timetools.Layout),
		input.End.Time.Format(timetools.Layout),
		input.AllDay,
		input.ID,
	)
	if err != nil {
		return model.Events{}, err
	}
	return dao.SelectEventByID(input.ID)
}

// DeleteEventByID perform a prepared DELETE statement against SQL db
func (dao *EventsDao) DeleteEventByID(id int) error {
	err := dao.conn.DBErr
	if nil != err {
		return err
	}
	delete, err := dao.conn.Instance.Prepare("DELETE FROM events WHERE id=?")
	if err != nil {
		return err
	}
	res, err := delete.Exec(id)
	if err != nil {
		return err
	}
	ra, err := res.RowsAffected()
	if err != nil {
		return err
	}
	if ra <= 0 {
		return fmt.Errorf("sql: no rows in result set")
	}
	return nil
}
