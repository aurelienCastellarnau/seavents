/*
 * File: router.go
 * Project: Seavents
 * File Created: Monday, 16th September 2019 7:07:44 pm
 * Author: Aurélien Castellarnau
 * Contact: (castellarnau.a@gmail.com)
 * -----
 * Last Modified: Saturday, 21st September 2019 6:30:10 pm
 * Modified By: Aurélien Castellarnau
 * -----
 * Copyright © 2019 - 2019 Castellarnau Aurélien, castellarnau aurélien
 */

package core

import (
	"encoding/json"
	"fmt"
	"net/http"
	"seavents/seavents/core/dao"

	"github.com/gorilla/mux"
)

// SeaventsSuccess a string for success response
type SeaventsSuccess struct {
	Message string `json:"message"`
}

// SeaventsError a code/string for error response
type SeaventsError struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

func handleError(code int, err error, w http.ResponseWriter) {
	seaventsError := SeaventsError{
		Code:    code,
		Message: err.Error(),
	}
	response, err := json.Marshal(seaventsError)
	if err != nil {
		fmt.Printf("\nError calling Marshal on Seavents Error... code: %d, message: %s", code, err.Error())
	}
	w.WriteHeader(code)
	fmt.Fprintf(w, "%s", response)
	return
}

// CustomHandler used as a Handler function by a HandlerWrapper,
// a CustomHandler accept an *App as parameter to help handle ResponseWriter and Request
type CustomHandler func(*App, http.ResponseWriter, *http.Request)

// HandlerWrapper implement http.Handler
// Can transmit an App to a CustomHandler
// Use these struct to share App throught an API
type HandlerWrapper struct {
	A *App
	H CustomHandler
}

// ServeHTTP is needed to implement http.Handler,
// it allows a HandlerWrapper to be processed by a *mux.Router,
// passing a *App among handlers.
func (hw HandlerWrapper) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	hw.H(hw.A, w, r)
}

// Wrap allow to initiate a HandlerWrapper chain
func (hw HandlerWrapper) Wrap(a *App, handler CustomHandler) *HandlerWrapper {
	return &HandlerWrapper{
		A: a,
		H: handler,
	}
}

// Road define an entry point in the API
type Road struct {
	Title   string
	Pattern string
	Method  string
	Handler CustomHandler
}

func optionHandler() {
}

// SetHeaders is implement:
// Cors request authorization
// application/json Content-Type
// see http://stackoverflow.com/questions/12830095/setting-http-headers-in-golang
func setHeaders(a *App, process *HandlerWrapper) *HandlerWrapper {
	return &HandlerWrapper{
		A: a,
		H: func(a *App, w http.ResponseWriter, r *http.Request) {
			if origin := r.Header.Get("Origin"); origin != "" {
				w.Header().Set("Access-Control-Allow-Origin", origin)
				w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
				w.Header().Set("Access-Control-Allow-Headers",
					"Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, Access-Control-Allow-Origin, Access-Control-Allow-Methods,Access-Control-Allow-Headers")
				w.Header().Set("Access-Control-Allow-Credentials", "true")
				w.Header().Set("Content-Type", "application/json")
			}
			if r.Method == "OPTIONS" {
				return
			}
			process.ServeHTTP(w, r)
		}}
}

// SetHeaders is implement:
// Cors request authorization
// application/json Content-Type
// see http://stackoverflow.com/questions/12830095/setting-http-headers-in-golang
func checkDatabase(a *App, process *HandlerWrapper) *HandlerWrapper {
	return &HandlerWrapper{
		A: a,
		H: func(a *App, w http.ResponseWriter, r *http.Request) {
			if r.Method == "OPTIONS" {
				return
			}
			if a.Database.DBErr != nil {
				a.Database.Instance, a.Database.DBErr = dao.Connect(a.BuildDSN())
			}
			process.ServeHTTP(w, r)
		}}
}

// CreateRouting build the a *mux.Router from a map of []Road
// indexed by a sub path (exemple: "/events") and return it
func (a *App) CreateRouting(roadSets map[string][]Road) *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	for section, r := range roadSets {
		subRouter := router.PathPrefix(section).Subrouter()
		for _, road := range r {
			wrapper := &HandlerWrapper{}
			h := wrapper.Wrap(a, road.Handler)
			h = checkDatabase(a, h)
			h = setHeaders(a, h)
			subRouter.Handle(road.Pattern, h).Methods("OPTIONS")
			subRouter.Handle(road.Pattern, h).Methods(road.Method)
		}
	}
	return router
}
