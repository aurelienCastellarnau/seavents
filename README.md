# WEMANITY KATA

## Dépôt gitlab:

https://gitlab.com/aurelienCastellarnau/seavents

### Contenu

Ce projet est à destination d'un ami et collègue plongeur. Prestataire dans le tourisme plongée, il souhaite disposer d'un outil spécialisé pour gérer ses réservations privées.
Il s'agit ici de l'API capable de gérer des évênements.

Pour l'API j'ai mis l'accents sur les tests et l'architecture. J'ai utilisé un maximum les outils, patterns et idiomes du Go.
Le projet est développé en intégration continue mais pas réellement en TDD dans la mesure ou test et fonctionnalités ont été développées en parallèle.

#### Pipelines du projet:

https://gitlab.com/aurelienCastellarnau/seavents/pipelines

#### Arborescence:

 - cases: les tests,
   - unit/ contient les tests qui adressent unitairements les fonctions sans le serveur lancé
   - functional/ contient les tests impliquant un client http, tests sur les routes
 - seavents: le coeur de l'app
   - core: ensemble dao/ configuration/ handler / router
   - model: la description des données, la manière de gérer le formatage Json/bytes
 - seavents.go, seavents_test.go: les 2 points d'entrée de l'app
 - benchmark.go: implémentation du benchmarking mis à dispo par golang

Autre:

 - cover/ doit exister pour recevoir les comptes-rendus du coverage
 - sql/ contient les script sql nécessaires au lancement

# Seavents

Event manager Golang/React

## Prerequisites

### API

Go 1.12

Developped with Golang 1.13 windows/amd64

Require Golang =>1.11 for usage of go modules
*****(Require Golang =>1.13 for usage of sql.NullTime but gitlab-ci sast stage support 1.12)*****

```https://golang.org/dl/```

Define your GOPATH.

```https://github.com/golang/go/wiki/SettingGOPATH```

### Client

https://gitlab.com/aurelienCastellarnau/seaventscalendar

## Installation

### API

In your GOPATH/src folder

```git clone https://gitlab.com/aurelienCastellarnau/seavents.git```

Then, use the provided compiled .exe file or rebuild to fit your needs.
With GOPTAH correctly defined:

```go build seavents```

## Testing

Tests run in an aleatory order.
Data are reset from the script in sql/seavent_test_db.sql

```mkdir -p cover && go test seavents/seavents -env=test -db seavent_test_db -v -timeout 120s -coverpkg=./... -covermode=count -coverprofile=./cover/coverage.cov -bench=. && go tool cover -html=./cover/coverage.cov```

## Launch

Run with the executable provided during build

execute with -h option to see args:

```
$ ./seavents.exe -h
Usage of C:\Projects\Gopath\src\seavents\seavents.exe:
  -bodysize int
        define the maxmimum accepted body size in kb (default 128)
  -db string
        define the host for sql database connection (default "seavent_db")
  -env string
        Environnement, default to dev, can be gitlab, test (localhost test) or prod (localhost) (default "dev")
  -ip string
        define ip address for API (default "127.0.0.1")
  -p string
        define port for API (default "4000")
  -sqlh string
        define the host for sql database connection (default "127.0.0.1")
  -sqlp string
        define the port for sql database connection (default "3306")
  -sqlpwd string
        define the password for sql database connection (default "root")
  -sqlto string
        define the timeout in sec for sql database connection (default "5")
  -sqlu string
        define the user for sql database connection (default "root")
```
